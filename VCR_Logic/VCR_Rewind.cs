﻿using System.Diagnostics;

namespace VCR_Logic
{
    public class VCR_Rewind : VCR_State
    {
        public VCR_Rewind(Tape_State tapeState) : base(tapeState)
        {
            EntryAction();
        }

        private void EntryAction()
        {
            Trace.WriteLine("Rewind State: start rewinding");
            VCR_State.OnTimerStart();
        }

        private void ExitAction()
        {
            Trace.WriteLine("Rewind State: stop rewinding");
            VCR_State.OnTimerStop();
        }

        public override VCR_State HandlePlay()
        {
            ExitAction();
            VCR_State nextState = new VCR_Play(_tapeState);
            return nextState;
        }

        public override VCR_State HandleStop()
        {
            ExitAction();
            VCR_State nextState = new VCR_Standby(_tapeState);
            return nextState;
        }

        public override VCR_State HandleFastForward()
        {
            ExitAction();
            VCR_State nextState = new VCR_FastForward(_tapeState);
            return nextState;
        }

        public override VCR_State HandleTimerEnd()
        {
            ExitAction();
            return base.HandleNotEndTape();
        }

        public override string ToString()
        {
            return "Rewind";
        }
    }
}