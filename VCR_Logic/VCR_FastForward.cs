﻿using System.Diagnostics;

namespace VCR_Logic
{
    public class VCR_FastForward : VCR_State
    {
        public VCR_FastForward(Tape_State tapeState) : base(tapeState)
        {
            EntryAction();
        }

        private void EntryAction()
        {
            Trace.WriteLine("Fast Forward State: start fast forwarding");
            VCR_State.OnTimerStart();
        }

        private void ExitAction()
        {
            Trace.WriteLine("Fast Forward State: stop fast forwarding");
            VCR_State.OnTimerStop();
        }

        public override VCR_State HandleRewind()
        {
            ExitAction();
            VCR_State nextState = new VCR_Rewind(_tapeState);
            return nextState;
        }

        public override VCR_State HandlePlay()
        {
            ExitAction();
            VCR_State nextState = new VCR_Play(_tapeState);
            return nextState;
        }

        public override VCR_State HandleStop()
        {
            ExitAction();
            VCR_State nextState = new VCR_Standby(_tapeState);
            return nextState;
        }

        public override VCR_State HandleTimerEnd()
        {
            ExitAction();
            return base.HandleEndTape();
        }

        public override string ToString()
        {
            return "Fast Forward";
        }
    }
}