﻿using System;

namespace VCR_Logic
{
    public class VCR_Standby : VCR_State
    {
        public VCR_Standby(Tape_State tapeState) : base(tapeState)
        {

        }

        public override VCR_State HandleRewind()
        {
            VCR_State nextState = base.HandleRewind();
            if (IsTapeIn() && IsTapeAtEnd())
            {
                nextState = new VCR_Rewind(_tapeState);
            }
            return nextState;
        }

        public override VCR_State HandlePlay()
        {
            VCR_State nextState = base.HandlePlay();
            if (IsTapeIn() && !IsTapeAtEnd())
            {
                nextState = new VCR_Play(_tapeState);
            }
            return nextState;
        }

        public override VCR_State HandleRecord()
        {
            VCR_State nextState = base.HandleRecord();
            if (IsTapeIn() && !IsTapeAtEnd())
            {
                nextState = new VCR_Record(_tapeState);
            }
            return nextState;
        }

        public override VCR_State HandleFastForward()
        {
            VCR_State nextState = base.HandleFastForward();
            if (IsTapeIn() && !IsTapeAtEnd())
            {
                nextState = new VCR_FastForward(_tapeState);
            }
            return nextState;
        }

        public override string ToString()
        {
            return "Standby";
        }
    }
}