﻿using System.Diagnostics;

namespace VCR_Logic
{
    public class VCR_Record : VCR_State
    {
        public VCR_Record(Tape_State tapeState) : base(tapeState)
        {
            EntryAction();
        }

        private void EntryAction()
        {
            Trace.WriteLine("Record State: start recording");
            VCR_State.OnTimerStart();
        }

        private void ExitAction()
        {
            Trace.WriteLine("Record State: stop recording");
            VCR_State.OnTimerStop();
        }

        public override VCR_State HandleStop()
        {
            ExitAction();
            VCR_State nextState = new VCR_Standby(_tapeState);
            return nextState;
        }
        
        public override VCR_State HandleTimerEnd()
        {
            ExitAction();
            return base.HandleEndTape();
        }

        public override string ToString()
        {
            return "Record";
        }
    }
}