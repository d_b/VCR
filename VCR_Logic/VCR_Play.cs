﻿using System.Diagnostics;

namespace VCR_Logic
{
    public class VCR_Play : VCR_State
    {
        public VCR_Play(Tape_State tapeState) : base(tapeState)
        {
            EntryAction();
        }

        private void EntryAction()
        {
            Trace.WriteLine("Play State: start playing");
            VCR_State.OnTimerStart();
        }

        private void ExitAction()
        {
            Trace.WriteLine("Play State: stop playing");
            VCR_State.OnTimerStop();
        }

        public override VCR_State HandleRewind()
        {
            ExitAction();
            VCR_State nextState = new VCR_Rewind(_tapeState);
            return nextState;
        }

        public override VCR_State HandleStop()
        {
            ExitAction();
            VCR_State nextState = new VCR_Standby(_tapeState);
            return nextState;
        }

        public override VCR_State HandleFastForward()
        {
            ExitAction();
            VCR_State nextState = new VCR_FastForward(_tapeState);
            return nextState;
        }

        public override VCR_State HandleTimerEnd()
        {
            ExitAction();
            return base.HandleEndTape();
        }

        public override string ToString()
        {
            return "Play";
        }
    }
}