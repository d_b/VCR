﻿using System.Reflection;
using System.Threading;
using Microsoft.Win32;

namespace VCR_Logic
{
    public enum Tape_State
    {
        NoTapeIn,
        TapeInNotEnd,
        TapeInEnd
    }

    public abstract class VCR_State
    {
        public delegate void TimerEventHandler();
        public static event TimerEventHandler TimerStart;
        public static event TimerEventHandler TimerStop;

        protected static bool TimerRunning = false;
        protected Tape_State _tapeState;

        public VCR_State(Tape_State tapeState)
        {
            _tapeState = tapeState;
        }

        protected static void OnTimerStart()
        {
            TimerRunning = true;
            TimerStart?.Invoke();
        }

        protected static void OnTimerStop()
        {
            TimerRunning = false;
            TimerStop?.Invoke();
        }

        public virtual VCR_State HandleRewind()
        {
            return this;
        }

        public virtual VCR_State HandlePlay()
        {
            return this;
        }

        public virtual VCR_State HandleRecord()
        {
            return this;
        }

        public virtual VCR_State HandleStop()
        {
            return this;
        }

        public virtual VCR_State HandleFastForward()
        {
            return this;
        }

        public virtual VCR_State HandleTimerEnd()
        {
            return this;
        }

        public virtual VCR_State HandleEndTape()
        {
            _tapeState = Tape_State.TapeInEnd;
            VCR_State nextState = new VCR_Standby(_tapeState);
            return nextState;
        }

        public virtual VCR_State HandleNotEndTape()
        {
            _tapeState = Tape_State.TapeInNotEnd;
            VCR_State nextState = new VCR_Standby(_tapeState);
            return nextState;
        }

        public bool TryToggleTape()
        {
            // do nothing is timer is running
            if (TimerRunning)
            {
                return false;
            }

            if (!IsTapeIn())
            {
                _tapeState = Tape_State.TapeInNotEnd;
            }
            else
            {
                _tapeState = Tape_State.NoTapeIn;
            }
            return true;
        }

        public bool IsTapeIn()
        {
            return _tapeState != Tape_State.NoTapeIn;
        }

        public bool IsTapeAtEnd()
        {
            return _tapeState == Tape_State.TapeInEnd;
        }

        public string TapeStateToString()
        {
            return _tapeState.ToString();
        }

        public abstract override string ToString();
    }
}