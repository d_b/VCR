﻿namespace VCR
{
    partial class VCR_GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.VCR_Timer = new System.Windows.Forms.Timer(this.components);
            this.Play_Button = new System.Windows.Forms.Button();
            this.Stop_Button = new System.Windows.Forms.Button();
            this.Record_Button = new System.Windows.Forms.Button();
            this.Rewind_Button = new System.Windows.Forms.Button();
            this.FastForward_Button = new System.Windows.Forms.Button();
            this.ToggleTape_Button = new System.Windows.Forms.Button();
            this.VCRState_ButtonAsLabel = new System.Windows.Forms.Button();
            this.TapeState_ButtonAsLabel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Play_Button
            // 
            this.Play_Button.BackColor = System.Drawing.Color.White;
            this.Play_Button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Play_Button.FlatAppearance.BorderSize = 2;
            this.Play_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Play_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Play_Button.Location = new System.Drawing.Point(24, 144);
            this.Play_Button.Name = "Play_Button";
            this.Play_Button.Size = new System.Drawing.Size(99, 51);
            this.Play_Button.TabIndex = 0;
            this.Play_Button.TabStop = false;
            this.Play_Button.Text = "Play";
            this.Play_Button.UseVisualStyleBackColor = false;
            this.Play_Button.Click += new System.EventHandler(this.Play_Button_Click);
            // 
            // Stop_Button
            // 
            this.Stop_Button.BackColor = System.Drawing.Color.White;
            this.Stop_Button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Stop_Button.FlatAppearance.BorderSize = 2;
            this.Stop_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Stop_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Stop_Button.Location = new System.Drawing.Point(141, 144);
            this.Stop_Button.Name = "Stop_Button";
            this.Stop_Button.Size = new System.Drawing.Size(99, 51);
            this.Stop_Button.TabIndex = 1;
            this.Stop_Button.TabStop = false;
            this.Stop_Button.Text = "Stop";
            this.Stop_Button.UseVisualStyleBackColor = false;
            this.Stop_Button.Click += new System.EventHandler(this.Stop_Button_Click);
            // 
            // Record_Button
            // 
            this.Record_Button.BackColor = System.Drawing.Color.White;
            this.Record_Button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Record_Button.FlatAppearance.BorderSize = 2;
            this.Record_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Record_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Record_Button.Location = new System.Drawing.Point(712, 110);
            this.Record_Button.Name = "Record_Button";
            this.Record_Button.Size = new System.Drawing.Size(99, 51);
            this.Record_Button.TabIndex = 2;
            this.Record_Button.TabStop = false;
            this.Record_Button.Text = "Record";
            this.Record_Button.UseVisualStyleBackColor = false;
            this.Record_Button.Click += new System.EventHandler(this.Record_Button_Click);
            // 
            // Rewind_Button
            // 
            this.Rewind_Button.BackColor = System.Drawing.Color.White;
            this.Rewind_Button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Rewind_Button.FlatAppearance.BorderSize = 2;
            this.Rewind_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Rewind_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rewind_Button.Location = new System.Drawing.Point(712, 167);
            this.Rewind_Button.Name = "Rewind_Button";
            this.Rewind_Button.Size = new System.Drawing.Size(99, 51);
            this.Rewind_Button.TabIndex = 3;
            this.Rewind_Button.TabStop = false;
            this.Rewind_Button.Text = "Rewind";
            this.Rewind_Button.UseVisualStyleBackColor = false;
            this.Rewind_Button.Click += new System.EventHandler(this.Rewind_Button_Click);
            // 
            // FastForward_Button
            // 
            this.FastForward_Button.BackColor = System.Drawing.Color.White;
            this.FastForward_Button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.FastForward_Button.FlatAppearance.BorderSize = 2;
            this.FastForward_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FastForward_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FastForward_Button.Location = new System.Drawing.Point(817, 167);
            this.FastForward_Button.Name = "FastForward_Button";
            this.FastForward_Button.Size = new System.Drawing.Size(99, 51);
            this.FastForward_Button.TabIndex = 4;
            this.FastForward_Button.TabStop = false;
            this.FastForward_Button.Text = "Fast Forward";
            this.FastForward_Button.UseVisualStyleBackColor = false;
            this.FastForward_Button.Click += new System.EventHandler(this.FastForward_Button_Click);
            // 
            // ToggleTape_Button
            // 
            this.ToggleTape_Button.BackColor = System.Drawing.Color.Crimson;
            this.ToggleTape_Button.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ToggleTape_Button.FlatAppearance.BorderSize = 2;
            this.ToggleTape_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ToggleTape_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToggleTape_Button.Location = new System.Drawing.Point(331, 123);
            this.ToggleTape_Button.Name = "ToggleTape_Button";
            this.ToggleTape_Button.Size = new System.Drawing.Size(268, 51);
            this.ToggleTape_Button.TabIndex = 5;
            this.ToggleTape_Button.TabStop = false;
            this.ToggleTape_Button.Text = "Toggle Tape";
            this.ToggleTape_Button.UseVisualStyleBackColor = false;
            this.ToggleTape_Button.Click += new System.EventHandler(this.InsertTape_Button_Click);
            // 
            // VCRState_ButtonAsLabel
            // 
            this.VCRState_ButtonAsLabel.BackColor = System.Drawing.Color.Silver;
            this.VCRState_ButtonAsLabel.Enabled = false;
            this.VCRState_ButtonAsLabel.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.VCRState_ButtonAsLabel.FlatAppearance.BorderSize = 2;
            this.VCRState_ButtonAsLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VCRState_ButtonAsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VCRState_ButtonAsLabel.Location = new System.Drawing.Point(244, 209);
            this.VCRState_ButtonAsLabel.Name = "VCRState_ButtonAsLabel";
            this.VCRState_ButtonAsLabel.Size = new System.Drawing.Size(203, 57);
            this.VCRState_ButtonAsLabel.TabIndex = 6;
            this.VCRState_ButtonAsLabel.TabStop = false;
            this.VCRState_ButtonAsLabel.UseVisualStyleBackColor = false;
            // 
            // TapeState_ButtonAsLabel
            // 
            this.TapeState_ButtonAsLabel.BackColor = System.Drawing.Color.Silver;
            this.TapeState_ButtonAsLabel.Enabled = false;
            this.TapeState_ButtonAsLabel.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.TapeState_ButtonAsLabel.FlatAppearance.BorderSize = 2;
            this.TapeState_ButtonAsLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TapeState_ButtonAsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TapeState_ButtonAsLabel.Location = new System.Drawing.Point(477, 209);
            this.TapeState_ButtonAsLabel.Name = "TapeState_ButtonAsLabel";
            this.TapeState_ButtonAsLabel.Size = new System.Drawing.Size(203, 57);
            this.TapeState_ButtonAsLabel.TabIndex = 7;
            this.TapeState_ButtonAsLabel.TabStop = false;
            this.TapeState_ButtonAsLabel.UseVisualStyleBackColor = false;
            // 
            // VCR_GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::VCR.Properties.Resources.VCR;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(933, 408);
            this.Controls.Add(this.TapeState_ButtonAsLabel);
            this.Controls.Add(this.VCRState_ButtonAsLabel);
            this.Controls.Add(this.ToggleTape_Button);
            this.Controls.Add(this.FastForward_Button);
            this.Controls.Add(this.Rewind_Button);
            this.Controls.Add(this.Record_Button);
            this.Controls.Add(this.Stop_Button);
            this.Controls.Add(this.Play_Button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "VCR_GUI";
            this.Text = "VCR";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer VCR_Timer;
        private System.Windows.Forms.Button Play_Button;
        private System.Windows.Forms.Button Stop_Button;
        private System.Windows.Forms.Button Record_Button;
        private System.Windows.Forms.Button Rewind_Button;
        private System.Windows.Forms.Button FastForward_Button;
        private System.Windows.Forms.Button ToggleTape_Button;
        private System.Windows.Forms.Button VCRState_ButtonAsLabel;
        private System.Windows.Forms.Button TapeState_ButtonAsLabel;
    }
}

