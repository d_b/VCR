﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VCR_Logic;

namespace VCR
{
    public partial class VCR_GUI : Form
    {
        private const int VCRTimerInterval = 2000;

        // default initialization of states:
        // VCR is in Stanby state and tape is not in
        private VCR_State _vcrState = new VCR_Standby(Tape_State.NoTapeIn);

        public VCR_GUI()
        {
            InitializeComponent();

            VCR_State.TimerStart += OnVCRWantsToStartTimer;
            VCR_State.TimerStop += OnVCRWantsToStoptimer;

            SetVCRStateLabel(_vcrState.ToString());
            SetTapeStateLabel(_vcrState.TapeStateToString());

            VCR_Timer.Interval = VCRTimerInterval;
            VCR_Timer.Tick += PlayTimerTick;
        }

        private void PlayTimerTick(object sender, EventArgs e)
        {
            Trace.WriteLine("VCR_Timer ended after " + VCR_Timer.Interval + " Milliseconds");
            _vcrState = _vcrState.HandleTimerEnd();
            SetVCRStateLabel(_vcrState.ToString());
            SetTapeStateLabel(_vcrState.TapeStateToString());
        }

        private void Play_Button_Click(object sender, EventArgs e)
        {
            _vcrState = _vcrState.HandlePlay();
            SetVCRStateLabel(_vcrState.ToString());
        }

        private void Stop_Button_Click(object sender, EventArgs e)
        {
            _vcrState = _vcrState.HandleStop();
            SetVCRStateLabel(_vcrState.ToString());
            SetTapeStateLabel(_vcrState.TapeStateToString());
        }

        private void InsertTape_Button_Click(object sender, EventArgs e)
        {
            if (_vcrState.TryToggleTape())
            {
                SetTapeStateLabel(_vcrState.TapeStateToString());
            }
        }

        private void Record_Button_Click(object sender, EventArgs e)
        {
            _vcrState = _vcrState.HandleRecord();
            SetVCRStateLabel(_vcrState.ToString());
            SetTapeStateLabel(_vcrState.TapeStateToString());
        }

        private void Rewind_Button_Click(object sender, EventArgs e)
        {
            _vcrState = _vcrState.HandleRewind();
            SetVCRStateLabel(_vcrState.ToString());
            SetTapeStateLabel(_vcrState.TapeStateToString());
        }

        private void FastForward_Button_Click(object sender, EventArgs e)
        {
            _vcrState = _vcrState.HandleFastForward();
            SetVCRStateLabel(_vcrState.ToString());
            SetTapeStateLabel(_vcrState.TapeStateToString());
        }

        private void SetVCRStateLabel(string state)
        {
            VCRState_ButtonAsLabel.Text = "VCR State: " + state;
        }

        private void SetTapeStateLabel(string state)
        {
            TapeState_ButtonAsLabel.Text = "Tape State: " + state;
        }

        private void OnVCRWantsToStartTimer()
        {
            VCR_Timer.Enabled = true;
        }

        private void OnVCRWantsToStoptimer()
        {
            VCR_Timer.Enabled = false;
        }
    }
}
